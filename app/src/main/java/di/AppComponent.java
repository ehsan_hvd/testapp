package di;

import com.example.hasanvand.testapp.MainActivity;

import dagger.Component;

/**
 * Created by Hasanvand on 1/31/2018.
 */
@Component(modules = AppModule.class)
public interface AppComponent {
    void inject(MainActivity mainActivity);
}
