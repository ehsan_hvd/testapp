package com.example.hasanvand.testapp;

/**
 * Created by Hasanvand on 1/31/2018.
 */

public interface MainActivityInterface extends MvpView {
    void feedBack(String text);
}
