package com.example.hasanvand.testapp;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hasanvand.testapp.databinding.ActivityMainBinding;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.inject.Inject;

import di.AppComponent;
import di.DaggerAppComponent;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class MainActivity extends AppCompatActivity implements MainActivityInterface {

    private final String TAG = "testTAG";
    @Inject
    MainPresenter presenter;

    private AppComponent component;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        User user = new User(1l, "ehsan", "hassanvand");
        binding.setUser(user);

        component = DaggerAppComponent.builder().build();
        component.inject(this);
        presenter.onAttach(this);
        binding.setPresenter(presenter);

        findViewById(R.id.btnTest).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, presenter.getText(), Toast.LENGTH_SHORT).show();
            }
        });

        Observable<Integer> observable
                = Observable.create(new ObservableOnSubscribe<Integer>() {
                                        @Override

                                        public void subscribe(ObservableEmitter<Integer> e) throws Exception {
                                            //Use onNext to emit each item in the stream//
                                            e.onNext(1);
                                            e.onNext(2);
                                            e.onNext(3);
                                            e.onNext(4);

                                            //Once the Observable has emitted all items in the sequence, call onComplete//
                                            e.onComplete();
                                        }
                                    }
        );

        Observer<Integer> observer = new Observer<Integer>() {
            @Override
            public void onSubscribe(Disposable d) {
                Log.d(TAG, "onSubscribe: ");
            }

            @Override
            public void onNext(Integer value) {
                Log.d(TAG, "onNext: " + value);
            }

            @Override
            public void onError(Throwable e) {
                Log.d(TAG, "onError: ");
            }

            @Override
            public void onComplete() {
                Toast.makeText(MainActivity.this, "RxJavaDone", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "onComplete: All Done!");
            }
        };

        //Create our subscription//
        observable.subscribe(observer);
        user.setPaid(true);

        findViewById(R.id.btnTap).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((TextView)findViewById(R.id.textTest)).setText(runAsRoot());
            }
        });

    }

    public String runAsRoot() {

        try {
            // Executes the command.
            Process process = Runtime.getRuntime().exec("/system/bin/input tap 50 300\n");

            // Reads stdout.
            // NOTE: You can write to stdin of the command using
            //       process.getOutputStream().
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));

            int read;
            char[] buffer = new char[4096];
            StringBuffer output = new StringBuffer();
            while ((read = reader.read(buffer)) > 0) {
                output.append(buffer, 0, read);
            }
            reader.close();

            // Waits for the command to finish.
            process.waitFor();

            return output.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void feedBack(String text) {
        Toast.makeText(MainActivity.this, text, Toast.LENGTH_SHORT).show();
    }
}
