package com.example.hasanvand.testapp;

/**
 * Created by ehsan on 2/11/18.
 */

public class BasePresenter {
    private MvpView view;

    public void onAttach(MvpView view){
        this.view = view;
    }

    public Object getView() {
        return view;
    }
}
